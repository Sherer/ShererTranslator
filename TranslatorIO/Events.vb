﻿Public Class DialogEventArgs : Inherits EventArgs

    Private _index As Integer
    Private _file As File

    Public Sub New(index As Integer, filename As String)
        _index = index
        _file = New File(filename)
    End Sub

    Public Sub New(index As Integer, file As File)
        _index = index
        _file = file
    End Sub

    Public ReadOnly Property Index As Integer
        Get
            Return _index
        End Get
    End Property
    Public ReadOnly Property File As File
        Get
            Return _file
        End Get
    End Property
End Class

Public Class FilterEventArgs : Inherits EventArgs

    Private _index As Integer
    Private _isChange As Boolean

    Public Sub New(index As Integer, isChange As Boolean)
        _index = index
        _isChange = isChange
    End Sub

    Public ReadOnly Property Index As Integer
        Get
            Return _index
        End Get
    End Property

    Public ReadOnly Property IsChange As Boolean
        Get
            Return _isChange
        End Get
    End Property

    Public ReadOnly Property IsValid As Boolean
        Get
            Return Me.Index > -1
        End Get
    End Property
End Class

Public Class CompleteEventArgs : Inherits EventArgs
    Private _state As Boolean
    Private _message As String

    Public Sub New(state As Boolean, message As String)
        Me.New(state, message, True)
    End Sub
    Public Sub New(state As Boolean, message As String, showMessage As Boolean)
        Me._state = state
        Me._message = message
        Me.ShowMessage = showMessage
    End Sub

    Public ReadOnly Property State As Boolean
        Get
            Return _state
        End Get
    End Property

    Public ReadOnly Property Message As String
        Get
            Return _message
        End Get
    End Property

    Public Property ShowMessage As Boolean
End Class