﻿Public Class DataFile
    Private _version As Integer
    Private _author As String
    Private _create As String
    Private _update As String
    Private _tags As List(Of String)
    Private _items As Dictionary(Of String, List(Of DataValue))

    Public Sub New()
        _tags = New List(Of String)
        _items = New Dictionary(Of String, List(Of DataValue))
    End Sub

    Public Property Version As Integer
        Get
            Return _version
        End Get
        Set(value As Integer)
            _version = value
        End Set
    End Property
    Public Property Author As String
        Get
            Return _author
        End Get
        Set(value As String)
            _author = value
        End Set
    End Property
    Public Property Create As String
        Get
            Return _create
        End Get
        Set(value As String)
            _create = value
        End Set
    End Property
    Public Property Update As String
        Get
            Return _update
        End Get
        Set(value As String)
            _update = value
        End Set
    End Property
    Public Property Tags As List(Of String)
        Get
            Return _tags
        End Get
        Set(value As List(Of String))
            _tags = value
        End Set
    End Property
    Public Property Items As Dictionary(Of String, List(Of DataValue))
        Get
            Return _items
        End Get
        Set(value As Dictionary(Of String, List(Of DataValue)))
            _items = value
        End Set
    End Property
End Class

Public Class DataValue : Implements IComparable(Of DataValue)
        Private _value As String
        Private _count As Integer
        Private _tags As List(Of Integer)

        Public Sub New()
            _tags = New List(Of Integer)
        End Sub
        Public Sub New(value As String, count As Integer, tags() As Integer)
            _value = value
            _count = count
            _tags = tags.ToList
        End Sub
        Public Sub New(value As String, count As Integer, tags As List(Of Integer))
            _value = value
            _count = count
            _tags = tags
        End Sub

        Public Function CompareTo(other As DataValue) As Integer Implements IComparable(Of DataValue).CompareTo
            If other.Count > Me.Count Then
                Return -1
            ElseIf other.Count = Me.Count Then
                Return 0
            Else
                Return 1
            End If
        End Function

        Public Property Value As String
            Get
                Return _value
            End Get
            Set(value As String)
                _value = value
            End Set
        End Property
        Public Property Count As Integer
            Get
                Return _count
            End Get
            Set(value As Integer)
                _count = value
            End Set
        End Property
        Public Property Tags As List(Of Integer)
            Get
                Return _tags
            End Get
            Set(value As List(Of Integer))
                _tags = value
            End Set
        End Property
End Class