﻿Public Class Row
    Public Sub New()
        Me.Mode = 1
    End Sub
    Public Sub New(index As Integer, number As Integer, source As String, type As Integer)
        Me.New()
        Me.Index = index
        Me.Number = number
        Me.Source = source
        Me.Type = type
    End Sub
    Public Sub New(index As Integer, number As Integer, key As String, source As String, type As Integer)
        Me.New(index, number, source, type)
        Me.Key = key
    End Sub
    Public Sub New(index As Integer, number As Integer, root As String, key As String, source As String, type As Integer)
        Me.New(index, number, key, source, type)
        Me.Root = root
    End Sub
    Public Sub New(index As Integer, number As Integer, root As String, key As String, source As String, comment As String, type As Integer)
        Me.New(index, number, root, key, source, type)
        Me.Comment = comment
    End Sub
    Public Sub New(index As Integer, number As Integer, root As String, key As String, source As String, comment As String, description As String, type As Integer)
        Me.New(index, number, root, key, source, comment, type)
        Me.Description = description
    End Sub
    Public Sub New(index As Integer, number As Integer, root As String, key As String, source As String, comment As String, type As Integer, mode As Integer)
        Me.New(index, number, root, key, source, comment, type)
        Me.Mode = mode
    End Sub

    Public Property Index As Integer
    Public Property Number As Integer
    Public Property Root As String
    Public Property Key As String
    Public Property Source As String
    Public Property Target As String
    Public Property Prefix As String
    Public Property Suffix As String
    Public Property Comment As String
    Public Property Description As String
    ''' <summary>
    ''' 0-Other
    ''' 1-Normal
    ''' 2-Root
    ''' 3-Comment
    ''' 4-Empty
    ''' 5-Header
    ''' 6-Multi Line
    ''' </summary>
    Public Property Type As Integer
    ''' <summary>
    ''' 1-Normal
    ''' 2-Work
    ''' </summary>
    Public Property Mode As Integer
    Public ReadOnly Property Show As Boolean
        Get
            Return Type = 1 OrElse Type = 6
        End Get
    End Property
End Class

Public Class State
    Public Sub New()
    End Sub
    Public Sub New(total As Integer, worked As Integer)
        Me.Total = total
        Me.Worked = worked
    End Sub

    Public Property Total As Integer
    Public Property Worked As Integer
    Public ReadOnly Property Unworked As Integer
        Get
            Return Me.Total - Me.Worked
        End Get
    End Property
    Public ReadOnly Property Rate As Single
        Get
            Return If(Me.Total = 0, 0, Val(Format((Me.Worked / Me.Total) * 100, "0.0")))
        End Get
    End Property
End Class

Public Class CompareResult
    Private _source As String
    Private _target As String
    Private _similarity As Double
    Private _tolerance As Double
    ''' <summary>
    ''' 1-Work
    ''' 2-Data
    ''' </summary>
    Private _from As Integer
    Private _isEqual As Boolean

    Public Sub New(isEqual As Boolean, similarity As Double, tolerance As Double)
        _isEqual = isEqual
        _similarity = similarity
        _tolerance = tolerance
    End Sub
    Public Sub New(source As String, target As String, similarity As Double, tolerance As Double, from As Integer)
            _source = source
            _target = target
            _similarity = similarity
            _tolerance = tolerance
            _from = from
        End Sub

    Public ReadOnly Property Source As String
        Get
            Return _source
        End Get
    End Property
    Public ReadOnly Property Target As String
        Get
            Return _target
        End Get
    End Property
    Public ReadOnly Property Similarity As Double
        Get
            Return _similarity
        End Get
    End Property
    Public ReadOnly Property Tolerance As Double
        Get
            Return _tolerance
        End Get
    End Property
    Public ReadOnly Property Rate As Double
        Get
            Return (Me.Similarity + (100 - Me.Tolerance)) / 2
        End Get
    End Property
    Public ReadOnly Property From As Integer
        Get
            Return _from
        End Get
    End Property
    Public ReadOnly Property IsEqual As Boolean
        Get
            Return _isEqual
        End Get
    End Property
End Class