﻿Public Class Filter
    Private _name As String
    Private _input() As String
    Private _output() As String
    Private _isIndex As Boolean
    Private _isHide As Boolean

    Public Sub New(name As String, input As String, Optional isIndex As Boolean = True, Optional isHide As Boolean = False)
        Me.New(name, {input}, {input}, isIndex, isHide)
    End Sub
    Public Sub New(name As String, input() As String, Optional isIndex As Boolean = True, Optional isHide As Boolean = False)
        Me.New(name, input, input, isIndex, isHide)
    End Sub
    Public Sub New(name As String, input As String, output As String, Optional isIndex As Boolean = True, Optional isHide As Boolean = False)
        Me.New(name, {input}, {output}, isIndex, isHide)
    End Sub
    Public Sub New(name As String, input() As String, output As String, Optional isIndex As Boolean = True, Optional isHide As Boolean = False)
        Me.New(name, input, {output}, isIndex, isHide)
    End Sub
    Public Sub New(name As String, input As String, output() As String, Optional isIndex As Boolean = True, Optional isHide As Boolean = False)
        Me.New(name, {input}, output, isIndex, isHide)
    End Sub
    Public Sub New(name As String, input() As String, output() As String, Optional isIndex As Boolean = True, Optional isHide As Boolean = False)
        _name = name
        _input = input
        _output = output
        _isIndex = isIndex
        _isHide = isHide
    End Sub

    Public ReadOnly Property Name As String
        Get
            Return _name
        End Get
    End Property
    Public ReadOnly Property Input As String()
        Get
            Return _input
        End Get
    End Property
    Public ReadOnly Property Output As String()
        Get
            Return _output
        End Get
    End Property
    Public ReadOnly Property IsIndex As Boolean
        Get
            Return _isIndex
        End Get
    End Property
    Public ReadOnly Property IsHide As Boolean
        Get
            Return _isHide
        End Get
    End Property
    Public ReadOnly Property InputString As String
        Get
            Return If(Me.Input.Length > 0, Me.Name & "|*" & Join(Me.Input, ";*"), "")
        End Get
    End Property
    Public ReadOnly Property Contains(extension As String) As Boolean
        Get
            Return Not Me.IsHide AndAlso Me.IsIndex AndAlso Me.Input.Contains(extension)
        End Get
    End Property
End Class
