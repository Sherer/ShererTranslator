﻿Imports System.IO
Imports NPOI.HSSF.UserModel
Imports NPOI.XSSF.UserModel
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json.JsonConvert
Imports System.Text.RegularExpressions

Public Class File
    Private _path As String
    Private _name As String
    Private _extension As String

    Public Sub New()
    End Sub
    Public Sub New(fileName As String)
        _path = fileName
        _name = GetFileName(fileName)
        _extension = GetFileExtension(fileName)
    End Sub

    Public Property Path As String
        Get
            Return _path
        End Get
        Set(value As String)
            _path = value
        End Set
    End Property
    Public Property Name As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    Public Property Extension As String
        Get
            Return _extension
        End Get
        Set(value As String)
            _extension = value
        End Set
    End Property
    Public ReadOnly Property FullName As String
        Get
            Return Me.Name & Me.Extension
        End Get
    End Property
End Class

Public Module FileModule

#Region ".lng"
    Public Function Readlng(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Dim strLine As String = Nothing
                Dim keyPair As String() = Nothing
                Dim number As Integer = 0
                Using reader As New IO.StreamReader(filename, Text.Encoding.Default, True)
                    While Not reader.EndOfStream
                        'Read Line
                        strLine = reader.ReadLine()

                        'Empty Line
                        If Trim(strLine) = "" Then
                            data.Add(data.Count, New Row(data.Count, 0, "", strLine, 4))
                            Continue While
                        End If

                        'Comments
                        If Trim(strLine).StartsWith("//", True, Globalization.CultureInfo.InvariantCulture) Then
                            data.Add(data.Count, New Row(data.Count, 0, "", strLine, 3))
                            Continue While
                        End If

                        'Other case
                        If Not strLine.Contains("=") Then
                            data.Add(data.Count, New Row(data.Count, 0, "", strLine, 0))
                            Continue While
                        End If

                        'Section, Key, Value
                        keyPair = strLine.Split({"="}, 2, StringSplitOptions.None)
                        Dim key As String = keyPair(0)
                        Dim value As String = If(keyPair.Length > 1, keyPair(1), "")
                        Dim comment As String = ""

                        'Inline comment
                        If value.Contains("//") Then
                            comment = value.Substring(value.IndexOf("//"))
                            value = value.Substring(0, value.IndexOf("//"))
                        End If

                        number += 1
                        data.Add(data.Count, New Row(data.Count, number, "", key, value, comment, 1))
                    End While
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub Savelng(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                For i = 0 To data.Count - 1
                    Dim row As Row = data(i)
                    If row.Type = 1 Then
                        writer.WriteLine(row.Key & "=" & row.Target & row.Comment)
                    Else
                        writer.WriteLine(row.Source)
                    End If
                Next
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".udraft"
    Private Const VERSION As Integer = 1
    Public Function ReadDraft(filename As String, ByRef file As File, ByRef index As Integer) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Using reader As New IO.StreamReader(filename, Text.Encoding.UTF8, True)
                    Dim Dict = DeserializeObject(Of Dictionary(Of String, Object))(reader.ReadToEnd)
                    If Not Dict.ContainsKey("file") OrElse Not Dict.ContainsKey("data") OrElse Not Dict.ContainsKey("version") Then
                        Throw New Exception("file is corrupted")
                    ElseIf Dict("version") > VERSION Then
                        Throw New Exception("version is too low")
                    Else
                        file = CType(Dict("file"), JObject).ToObject(Of File)
                        index = If(Dict.ContainsKey("index"), Dict("index"), -1)
                        data = CType(Dict("data"), JObject).ToObject(Of Dictionary(Of Integer, Row))
                    End If
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveDraft(filename As String, file As File, data As Dictionary(Of Integer, Row), Optional index As Integer = -1)
        Dim Dict As New Dictionary(Of String, Object) From {
            {"file", file},
            {"data", data},
            {"version", VERSION},
            {"index", index}
        }

        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                writer.Write(SerializeObject(Dict))
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".srt"
    Public Function ReadSrt(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Dim regex As New Regex("(?<sequence>\d+)\r\n(?<start>\d{2}\:\d{2}\:\d{2},\d{3}) --\> (?<end>\d{2}\:\d{2}\:\d{2},\d{3})\r\n(?<text>[\s\S]*?\r\n\r\n)", RegexOptions.Compiled Or RegexOptions.ECMAScript)
        Try
            If IO.File.Exists(filename) Then
                Using reader As New IO.StreamReader(filename, Text.Encoding.UTF8, True)
                    Dim number As Integer = 0
                    For Each match As Match In regex.Matches(reader.ReadToEnd)
                        number += 1
                        Dim sequence As Integer = Val(match.Groups("sequence").Value)
                        Dim time As String = match.Groups("start").Value & " --> " & match.Groups("end").Value
                        Dim text As String = match.Groups("text").Value.TrimEnd({ChrW(AscW(vbCr)), ChrW(AscW(vbLf)), ChrW(AscW(vbCrLf))})
                        data.Add(data.Count, New Row(data.Count, number, sequence, time, text, 1))
                    Next
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveSrt(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                For Each row In data
                    writer.WriteLine(row.Value.Root & vbCrLf & row.Value.Key & vbCrLf & row.Value.Target & vbCrLf)
                Next
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".ass"
    Public Function ReadAss(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Dim strLine As String = Nothing
                Dim root As String = Nothing
                Dim column As Integer = -1
                Dim number As Integer = 0
                Dim count As Integer = 0
                Using reader As New IO.StreamReader(filename, Text.Encoding.UTF8, True)
                    While Not reader.EndOfStream
                        'Read Line
                        strLine = reader.ReadLine()

                        'Empty Line
                        If Trim(strLine) = "" Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 4))
                            Continue While
                        End If

                        'Comment
                        If Trim(strLine).StartsWith(";", True, Globalization.CultureInfo.InvariantCulture) OrElse Trim(strLine).StartsWith("#", True, Globalization.CultureInfo.InvariantCulture) Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, strLine.TrimStart().Substring(1), 3))
                            Continue While
                        End If

                        'Root
                        If Trim(strLine).StartsWith("[", True, Globalization.CultureInfo.InvariantCulture) AndAlso Trim(strLine).EndsWith("]", True, Globalization.CultureInfo.InvariantCulture) Then
                            root = Trim(strLine.Substring(1, strLine.Length - 2))
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 2))
                            Continue While
                        End If

                        'Other
                        If root.ToLower <> "events" OrElse Not strLine.Contains(":") Then
OTHER:
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 0))
                            Continue While
                        End If

                        Dim arr = strLine.Split({":"}, 2, StringSplitOptions.None)
                        Dim key As String = arr(0)
                        Dim value As String = If(arr.Length > 1, arr(1), "")
                        If Trim(key).ToLower = "format" Then
                            Dim cols = value.Split(",").ToList
                            count = cols.Count
                            column = cols.FindIndex(Function(match) Trim(match).ToLower = "text")
                            data.Add(data.Count, New Row(data.Count, 0, root, key, strLine, value, 5))
                            Continue While
                        ElseIf Trim(key).ToLower = "dialogue" AndAlso column > -1 Then
                            Dim cols = value.Split({","c}, count).ToList
                            If cols.Count > column Then
                                Dim text = cols(column)
                                Dim reg As New Regex("\{(\S*)\}")
                                Dim match = reg.Match(text)
                                Dim desc As String = ""
                                If (match.Index = 0) Then
                                    desc = match.Value
                                    text = text.Substring(match.Length)
                                End If

                                cols(column) = "@@"
                                number += 1
                                data.Add(data.Count, New Row(data.Count, number, root, key, text, Join(cols.ToArray, ","), desc, 1))
                                Continue While
                            Else
                                GoTo OTHER
                            End If
                        Else
                            GoTo OTHER
                        End If
                    End While
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveAss(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                Dim column As Integer = -1
                For i = 0 To data.Count - 1
                    Dim row As Row = data(i)
                    If row.Type = 5 Then
                        Dim cols = row.Comment.Split(",").ToList
                        column = cols.FindIndex(Function(match) Trim(match).ToLower = "text")
                        writer.WriteLine(row.Source)
                    ElseIf row.Type = 1 Then
                        Dim cols = row.Comment.Split(",").ToList
                        If cols.Count > column Then
                            cols(column) = row.Description & row.Target
                            writer.WriteLine(row.Key & ":" & Join(cols.ToArray, ","))
                        Else
                            writer.WriteLine(row.Source)
                        End If
                    Else
                        writer.WriteLine(row.Source)
                    End If
                Next
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".ini"
    Public Function ReadIni(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Dim strLine As String = Nothing
                Dim root As String = Nothing
                Dim number As Integer = 0
                Using reader As New IO.StreamReader(filename, Text.Encoding.UTF8, True)
                    While Not reader.EndOfStream
                        'Read Line
                        strLine = reader.ReadLine()

                        'Empty Line
                        If Trim(strLine) = "" Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 4))
                            Continue While
                        End If

                        'Comment
                        If Trim(strLine).StartsWith(";", True, Globalization.CultureInfo.InvariantCulture) OrElse Trim(strLine).StartsWith("#", True, Globalization.CultureInfo.InvariantCulture) OrElse Trim(strLine).StartsWith("//", True, Globalization.CultureInfo.InvariantCulture) Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, strLine.TrimStart().Substring(1), 3))
                            Continue While
                        End If

                        'Root
                        If Trim(strLine).StartsWith("[", True, Globalization.CultureInfo.InvariantCulture) AndAlso Trim(strLine).EndsWith("]", True, Globalization.CultureInfo.InvariantCulture) Then
                            root = Trim(strLine.Substring(1, strLine.Length - 2))
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 2))
                            Continue While
                        End If

                        'Other
                        If Not strLine.Contains("=") Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 0))
                            Continue While
                        End If

                        'Data
                        number += 1
                        Dim arr = strLine.Split({"="}, 2, StringSplitOptions.None)
                        data.Add(data.Count, New Row(data.Count, number, root, arr(0), If(arr.Length > 1, arr(1), ""), 1))
                    End While
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveIni(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                For i = 0 To data.Count - 1
                    Dim row As Row = data(i)
                    If row.Type = 1 Then
                        writer.WriteLine(row.Key & "=" & row.Target)
                    Else
                        writer.WriteLine(row.Source)
                    End If
                Next
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".ini1"
    Public Function ReadIni1(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Dim reg As New Regex("\S(.*\S)")
                Dim strLine As String = Nothing
                Dim root As String = Nothing
                Dim multiKey As String = ""
                Dim number As Integer = 0
                Dim index As Integer = 0
                Using reader As New IO.StreamReader(filename, Text.Encoding.UTF8, True)
                    While Not reader.EndOfStream
                        'Read Line
                        strLine = reader.ReadLine()
                        Dim match As Match = reg.Match(strLine)

                        'Empty Line
                        If Not match.Success Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 4))
                            Continue While
                        End If

                        'Comment
                        If match.Value.StartsWith(";", True, Globalization.CultureInfo.InvariantCulture) OrElse match.Value.StartsWith("#", True, Globalization.CultureInfo.InvariantCulture) OrElse match.Value.StartsWith("//", True, Globalization.CultureInfo.InvariantCulture) Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, "", 3))
                            index = 0
                            Continue While
                        End If

                        'Root
                        If match.Value.StartsWith("[", True, Globalization.CultureInfo.InvariantCulture) AndAlso match.Value.EndsWith("]", True, Globalization.CultureInfo.InvariantCulture) Then
                            root = Trim(match.Value.Substring(1, match.Value.Length - 2))
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 2))
                            index = 0
                            Continue While
                        End If

                        'Multi line data
                        If match.Value.StartsWith("""", True, Globalization.CultureInfo.InvariantCulture) AndAlso match.Value.EndsWith("""", True, Globalization.CultureInfo.InvariantCulture) AndAlso index > 0 Then
                            index += 1
                            number += 1
                            data.Add(data.Count, New Row(data.Count, number, root, multiKey & "_ROW_" & index, match.Value.Substring(1, match.Value.Length - 2), 6) With {.Prefix = strLine.Substring(0, match.Index + 1), .Suffix = strLine.Substring(match.Index + match.Length - 1)})
                            Continue While
                        End If

                        'Other
                        If Not strLine.Contains("=") Then
                            data.Add(data.Count, New Row(data.Count, 0, root, "", strLine, 0))
                            Continue While
                        End If

                        'Data
                        number += 1
                        Dim arr = strLine.Split({"="}, 2, StringSplitOptions.None)
                        multiKey = arr(0)
                        index = 1
                        If arr.Length <= 1 Then
                            data.Add(data.Count, New Row(data.Count, number, root, arr(0), "", "", "", 1))
                        Else
                            match = reg.Match(arr(1))
                            If match.Value.StartsWith("""", True, Globalization.CultureInfo.InvariantCulture) AndAlso match.Value.EndsWith("""", True, Globalization.CultureInfo.InvariantCulture) Then
                                data.Add(data.Count, New Row(data.Count, number, root, arr(0), match.Value.Substring(1, match.Value.Length - 2), 1) With {.Prefix = arr(1).Substring(0, match.Index + 1), .Suffix = arr(1).Substring(match.Index + match.Length - 1)})
                            Else
                                data.Add(data.Count, New Row(data.Count, number, root, arr(0), arr(1), 1) With {.Prefix = "", .Suffix = ""})
                            End If
                        End If
                    End While
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveIni1(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                For i = 0 To data.Count - 1
                    Dim row As Row = data(i)
                    If row.Type = 1 Then
                        writer.WriteLine(row.Key & "=" & row.Prefix & row.Target & row.Suffix)
                    ElseIf row.Type = 6 Then
                        writer.WriteLine(row.Prefix & row.Target & row.Suffix)
                    Else
                        writer.WriteLine(row.Source)
                    End If
                Next
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".xls"
    Public Function ReadXls(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Using file As New FileStream(filename, FileMode.Open, FileAccess.Read)
                    Dim workbook As New HSSFWorkbook(file)
                    Dim sheet As HSSFSheet = workbook.GetSheetAt(0)
                    For i As Integer = sheet.FirstRowNum To sheet.LastRowNum
                        Dim row As HSSFRow = sheet.GetRow(i)

                        Select Case row.LastCellNum
                            Case Is < 1 : Continue For
                            Case 1 : data.Add(data.Count, New Row(data.Count, data.Count, row.GetCell(0).ToString(), 1))
                            Case 2 : data.Add(data.Count, New Row(data.Count, data.Count, row.GetCell(0).ToString(), row.GetCell(1).ToString(), 1))
                            Case Else
                                Dim list As New List(Of String)
                                For j = 2 To row.LastCellNum - 1 : list.Add(row.GetCell(j).ToString) : Next
                                data.Add(data.Count, New Row(data.Count, data.Count, "", row.GetCell(0).ToString(), row.GetCell(1).ToString(), SerializeObject(list), 1))
                        End Select
                    Next
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveXls(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Dim workbook As New HSSFWorkbook()
            Dim sheet = workbook.CreateSheet("sheet1")

            For i = 0 To data.Count - 1
                Dim row As Row = data(i)
                If row.Type = 1 Then
                    Dim datarow = sheet.CreateRow(i)
                    Dim datacell = datarow.CreateCell(0)
                    If row.Key = "" Then
                        datacell.SetCellValue(row.Target)
                        Continue For
                    Else
                        datacell.SetCellValue(row.Key)
                        datacell = datarow.CreateCell(1)
                        datacell.SetCellValue(row.Target)
                        If row.Comment <> "" Then
                            Try
                                Dim list As New List(Of String)
                                list = DeserializeObject(Of List(Of String))(row.Comment)
                                For j = 2 To list.Count - 1
                                    datacell = datarow.CreateCell(j)
                                    datacell.SetCellValue(list(j - 2))
                                Next
                            Catch ex As Exception : Continue For : End Try
                        End If
                    End If
                End If
            Next

            Using fs As New FileStream(filename, FileMode.Create, FileAccess.Write)
                workbook.Write(fs)
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".xlsx"
    Public Function ReadXlsx(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Using file As New FileStream(filename, FileMode.Open, FileAccess.Read)
                    Dim workbook As New XSSFWorkbook(file)
                    Dim sheet As XSSFSheet = workbook.GetSheetAt(0)
                    For i As Integer = sheet.FirstRowNum To sheet.LastRowNum
                        Dim row As XSSFRow = sheet.GetRow(i)

                        Select Case row.LastCellNum
                            Case Is < 1 : Continue For
                            Case 1 : data.Add(data.Count, New Row(data.Count, data.Count, row.GetCell(0).ToString(), 1))
                            Case 2 : data.Add(data.Count, New Row(data.Count, data.Count, row.GetCell(0).ToString(), row.GetCell(1).ToString(), 1))
                            Case Else
                                Dim list As New List(Of String)
                                For j = 2 To row.LastCellNum - 1 : list.Add(row.GetCell(j).ToString) : Next
                                data.Add(data.Count, New Row(data.Count, data.Count, "", row.GetCell(0).ToString(), row.GetCell(1).ToString(), SerializeObject(list), 1))
                        End Select
                    Next
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveXlsx(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Dim workbook As New XSSFWorkbook()
            Dim sheet = workbook.CreateSheet("sheet1")

            For i = 0 To data.Count - 1
                Dim row As Row = data(i)
                If row.Type = 1 Then
                    Dim datarow = sheet.CreateRow(i)
                    Dim datacell = datarow.CreateCell(0)
                    If row.Key = "" Then
                        datacell.SetCellValue(row.Target)
                        Continue For
                    Else
                        datacell.SetCellValue(row.Key)
                        datacell = datarow.CreateCell(1)
                        datacell.SetCellValue(row.Target)
                        If row.Comment <> "" Then
                            Try
                                Dim list As New List(Of String)
                                list = DeserializeObject(Of List(Of String))(row.Comment)
                                For j = 2 To list.Count - 1
                                    datacell = datarow.CreateCell(j)
                                    datacell.SetCellValue(list(j - 2))
                                Next
                            Catch ex As Exception : Continue For : End Try
                        End If
                    End If
                End If
            Next

            Using fs As New FileStream(filename, FileMode.Create, FileAccess.Write)
                workbook.Write(fs)
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

#Region ".xtf"
    Private Const XTF_SPRATOR As String = "	"
    Public Function ReadXtf(filename As String) As Dictionary(Of Integer, Row)
        Dim data As New Dictionary(Of Integer, Row)
        Try
            If IO.File.Exists(filename) Then
                Dim strLine As String = Nothing
                Dim keyPair As String() = Nothing
                Dim number As Integer = 0
                Using reader As New IO.StreamReader(filename, Text.Encoding.Default, True)
                    While Not reader.EndOfStream
                        'Read Line
                        strLine = reader.ReadLine()

                        'Empty Line
                        If Trim(strLine) = "" Then
                            data.Add(data.Count, New Row(data.Count, 0, "", strLine, 4))
                            Continue While
                        End If

                        'Other case
                        If Not strLine.Contains(XTF_SPRATOR) Then
                            data.Add(data.Count, New Row(data.Count, 0, "", strLine, 0))
                            Continue While
                        End If

                        'Section, Key, Value
                        keyPair = strLine.Split({XTF_SPRATOR}, StringSplitOptions.None)
                        Dim key As String = keyPair(0)
                        Dim source As String = If(keyPair.Length > 1, keyPair(1), "")
                        Dim description As String = If(keyPair.Length > 2, keyPair(2), "")
                        Dim Comment As String = If(keyPair.Length > 3, keyPair(3), "")

                        number += 1
                        data.Add(data.Count, New Row(data.Count, number, "", key, source, Comment, description, 1))
                    End While
                End Using
            End If
        Catch ex As Exception : Throw ex : End Try
        Return data
    End Function
    Public Sub SaveXtf(filename As String, data As Dictionary(Of Integer, Row))
        Try
            Using writer As New IO.StreamWriter(filename, False, Text.Encoding.UTF8)
                For i = 0 To data.Count - 1
                    Dim row As Row = data(i)
                    If row.Type = 1 Then
                        writer.WriteLine(row.Key & XTF_SPRATOR & row.Target & XTF_SPRATOR & row.Description & XTF_SPRATOR & row.Comment)
                    Else
                        writer.WriteLine(row.Source)
                    End If
                Next
            End Using
        Catch ex As Exception : Throw ex : End Try
    End Sub
#End Region

End Module