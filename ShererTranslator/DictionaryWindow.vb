﻿Imports ShererTranslator.MessageBox
Public Class DictionaryWindow

#Region "Variables"
    Dim actived As Boolean
#End Region

#Region "UI Language"
    Sub New()

#Region "This call is required by the designer."
        actived = False
        InitializeComponent()

        Dim CheckButton As New ToolStripSplitCheckButton
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainWindow))
        CheckButton.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        CheckButton.DropDownButtonWidth = 0
        CheckButton.Alignment = ToolStripItemAlignment.Right
        AddHandler CheckButton.ButtonClick,
            Sub()
                Me.Owner = If(Me.Owner Is Nothing, MainWindow, Nothing)
                CheckButton.Checked = If(Me.Owner Is Nothing, False, True)
            End Sub
        StatusStrip1.Items.Add(CheckButton)

        actived = True
#End Region

#Region "Set fonts"
        Dim generalFont As Font = New FontConverter().ConvertFromString(Language("fonts", "general"))
        Dim menuFont As Font = New FontConverter().ConvertFromString(Language("fonts", "menu"))

        Me.Font = generalFont
        Me.StatusStrip1.Font = menuFont
#End Region

#Region "Set right to left"
        Me.RightToLeft = R2L
        Me.RightToLeftLayout = R2L
#End Region

#Region "Set window words"
        Me.Text = Language("dictionary", "title")
        Label1.Text = Language("dictionary", "key")
        Label2.Text = Language("dictionary", "source")
        Label3.Text = Language("dictionary", "target")
        Label4.Text = Language("dictionary", "tags")

        Button1.Text = Language("dictionary", "search")
        Button2.Text = Language("dictionary", "add")
        Button3.Text = Language("dictionary", "edit")
        Button4.Text = Language("dictionary", "delete")

        ToolStripStatusLabel1.Text = Language("dictionary", "record")
#End Region

    End Sub
#End Region

    Private Sub DictionaryWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Label4.Visible = False
        'TextBox4.Visible = False

        RecordLabel.Text = 0
        Button1.Height = TextBox1.Height

        TextBox1.Focus()
        TextBox1.Select()
        Button1.PerformClick()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim key As String = Trim(TextBox1.Text)

        Dim Dt As New DataTable
        Dt.Columns.Add("Source")
        Dt.Columns.Add("Target")
        Dt.Columns.Add("Tags")

        If key <> "" Then
            For Each item In DB.GetContainsList(key)
                Dt.Rows.Add({item.Key, item.Value.Value, Join(DB.GetTags(item.Value.Tags), ";")})
            Next
        End If

        RecordLabel.Text = Dt.Rows.Count
        DataGridView1.DataSource = Dt
        DataGridView1.Columns.Item("Tags").Visible = False
        DataGridView1.Columns.Item("Source").HeaderText = Language("dictionary", "source_column")
        DataGridView1.Columns.Item("Target").HeaderText = Language("dictionary", "target_column")
    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            Button1.PerformClick()
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1 Is Nothing OrElse DataGridView1.RowCount = 0 OrElse DataGridView1.SelectedRows.Count = 0 Then Return
        With DataGridView1.SelectedRows(0)
            TextBox2.Text = .Cells("Source").Value.ToString
            TextBox3.Text = .Cells("Target").Value.ToString
            TextBox4.Text = .Cells("Tags").Value.ToString
        End With
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim Source As String = TextBox2.Text
        Dim Target As String = TextBox3.Text
        Dim Tags() As String = TextBox4.Text.Split(SplitStr.ToCharArray, StringSplitOptions.RemoveEmptyEntries)

        If Source = "" OrElse Target = "" OrElse DataGridView1 Is Nothing Then Return
        DB.Add(Source, Target, Tags, True,
                New EventHandler(Of TranslatorIO.CompleteEventArgs)(
                Sub(obj, arg)
                    If arg.State Then
                        CType(DataGridView1.DataSource, DataTable).Rows.Add({Source, Target, Join(Tags, ";")})
                        MsgBox(Language("message_content", "add_content"), Language("message_title", "add_content"), MsgBoxOptions)
                    Else
                        MsgBox(String.Format(Language("message_content", "add_content_error"), arg.Message), MsgBoxStyle.Critical, Language("message_title", "add_content"), MsgBoxOptions)
                    End If
                End Sub))
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim Source As String = TextBox2.Text
        Dim Target As String = TextBox3.Text
        Dim Tags() As String = TextBox4.Text.Split(SplitStr.ToCharArray, StringSplitOptions.RemoveEmptyEntries)

        If Source = "" OrElse Target = "" OrElse DataGridView1 Is Nothing OrElse DataGridView1.RowCount = 0 OrElse DataGridView1.SelectedRows.Count = 0 Then Return
        With DataGridView1.SelectedRows(0)
            DB.Edit(.Cells("Source").Value.ToString, .Cells("Target").Value.ToString, Source, Target, Tags, True,
                    New EventHandler(Of TranslatorIO.CompleteEventArgs)(
                    Sub(obj, arg)
                        If arg.State Then
                            .Cells("Source").Value = Source
                            .Cells("Target").Value = Target
                            .Cells("Tags").Value = Join(Tags, ";")

                            MsgBox(Language("message_content", "edit_content"), Language("message_title", "edit_content"), MsgBoxOptions)
                        Else
                            MsgBox(String.Format(Language("message_content", "edit_content_error"), arg.Message), MsgBoxStyle.Critical, Language("message_title", "edit_content"), MsgBoxOptions)
                        End If
                    End Sub))
        End With
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If DataGridView1 Is Nothing OrElse DataGridView1.RowCount = 0 OrElse DataGridView1.SelectedRows.Count = 0 Then Return
        With DataGridView1.SelectedRows(0)
            DB.Remove(.Cells("Source").Value, .Cells("Target").Value, True,
                        New EventHandler(Of TranslatorIO.CompleteEventArgs)(
                        Sub(obj, arg)
                            If arg.State Then
                                DataGridView1.Rows.RemoveAt(.Index)
                                MsgBox(Language("message_content", "delete_content"), Language("message_title", "delete_content"), MsgBoxOptions)
                            Else
                                MsgBox(String.Format(Language("message_content", "delete_content_error"), arg.Message), MsgBoxStyle.Critical, Language("message_title", "delete_content"), MsgBoxOptions)
                            End If
                        End Sub))
        End With
    End Sub

End Class