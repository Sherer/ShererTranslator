﻿Imports System.Text.RegularExpressions

Public Class SearchWindow

#Region "Variables"
    Dim search As Boolean
    Dim actived As Boolean
    Dim index As Integer
    Dim state As Boolean
#End Region

#Region "UI Language"
    Sub New()

#Region "This call is required by the designer."
        actived = False
        InitializeComponent()
        actived = True
#End Region

#Region "Set fonts"
        Dim generalFont As Font = New FontConverter().ConvertFromString(Language("fonts", "general"))
        Me.Font = generalFont
#End Region

#Region "Set right to left"
        Me.RightToLeft = R2L
        Me.RightToLeftLayout = R2L
#End Region

#Region "Set window words"
        'Me.Text = Language("search_replace", Me.Tag.ToString)
        Label1.Text = Language("search_replace", "search_text")
        Label2.Text = Language("search_replace", "replace_text")

        CheckBox1.Text = Language("search_replace", "ignore_case")
        CheckBox2.Text = Language("search_replace", "wrap_around")
        CheckBox3.Text = Language("search_replace", "whole_words_only")
        CheckBox4.Text = Language("search_replace", "find_key")
        CheckBox5.Text = Language("search_replace", "find_source")
        CheckBox6.Text = Language("search_replace", "find_target")

        Button1.Text = Language("search_replace", "next")
        Button2.Text = Language("search_replace", "previous")
        Button3.Text = Language("search_replace", "replace")
        Button4.Text = Language("search_replace", "replace_all")
#End Region

    End Sub
#End Region

#Region "Basic"
    Private Sub SearchWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        state = False
        index = MainWindow.ListView1.SelectedItems(0).Index
        TextBox1_TextChanged(TextBox1, e)
    End Sub
    Private Sub SearchWindow_TextChanged(sender As Object, e As EventArgs) Handles MyBase.TextChanged
        If Not actived Then Return
        Select Case Me.Tag
            Case "search"
                search = True
                CheckBox4.Enabled = True
                CheckBox5.Enabled = True
                CheckBox6.Enabled = True
                CheckBox6.Checked = True
                Button3.Visible = False
                Button4.Visible = False
                Label2.Visible = False
                TextBox2.Visible = False
            Case "replace"
                search = False
                CheckBox4.Checked = False
                CheckBox5.Checked = False
                CheckBox6.Checked = True
                CheckBox4.Enabled = False
                CheckBox5.Enabled = False
                CheckBox6.Enabled = False
                Button3.Visible = True
                Button4.Visible = True
                Label2.Visible = True
                TextBox2.Visible = True
        End Select
    End Sub
    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        If TextBox1.Text = "" Then
            Button1.Enabled = False
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False
        Else
            Button1.Enabled = True
            Button2.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
        End If
    End Sub
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox6.CheckedChanged, CheckBox5.CheckedChanged, CheckBox4.CheckedChanged, CheckBox3.CheckedChanged, CheckBox2.CheckedChanged, CheckBox1.CheckedChanged
        Button1.Enabled = True
        Button2.Enabled = True
        Button3.Enabled = True
        Button4.Enabled = True
    End Sub

    Private Sub Search_Click(sender As Object, e As EventArgs) Handles Button1.Click, Button2.Click
        If (Not CheckBox4.Checked AndAlso Not CheckBox5.Checked AndAlso Not CheckBox6.Checked) OrElse TextBox1.Text = "" Then
            Button1.Enabled = False
            Button2.Enabled = False
            Return
        End If

        Dim search As String = If(CheckBox3.Checked, $"\b{TextBox1.Text}\b", TextBox1.Text)
        Dim regex As New Regex(search, If(CheckBox1.Checked, RegexOptions.IgnoreCase, RegexOptions.None))
        Dim count As Integer = MainWindow.FileSystem.GetValid.LongCount(Function(item) (CheckBox4.Checked AndAlso regex.IsMatch(Trim(item.Value.Key))) OrElse (CheckBox5.Checked AndAlso regex.IsMatch(Trim(item.Value.Source))) OrElse (CheckBox6.Checked AndAlso regex.IsMatch(Trim(item.Value.Target))))

        If count = 0 Then
            Button1.Enabled = False
            Button2.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False
            Return
        ElseIf count > 1 Then
            Button1.Enabled = True
            Button2.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
        End If
REWORK:
        Dim ends, steps As Integer
        If sender Is Button1 Then
            ends = MainWindow.ListView1.Items.Count - 1
            steps = 1
        ElseIf sender Is Button2 Then
            ends = 0
            steps = -1
        Else
            Return
        End If

        For i = index To ends Step steps
            Dim iindex As Integer = MainWindow.ListView1.Items(i).Text
            Dim item As TranslatorIO.Row = MainWindow.FileSystem.Work(iindex)
            If (CheckBox4.Checked AndAlso regex.IsMatch(Trim(item.Key))) OrElse (CheckBox5.Checked AndAlso regex.IsMatch(Trim(item.Source))) OrElse (CheckBox6.Checked AndAlso regex.IsMatch(Trim(item.Target))) Then

                MainWindow.ListView1.Items(i).Selected = True
                MainWindow.ListView1.EnsureVisible(i)
                index = i + steps

                If CheckBox5.Checked Then
                    For Each match As Match In regex.Matches(Trim(item.Source))
                        MainWindow.RichTextBox1.SelectionStart = match.Index
                        MainWindow.RichTextBox1.SelectionLength = match.Length
                        Exit For
                    Next
                End If

                If CheckBox6.Checked Then
                    For Each match As Match In regex.Matches(Trim(item.Target))
                        MainWindow.RichTextBox2.SelectionStart = match.Index
                        MainWindow.RichTextBox2.SelectionLength = match.Length
                        Exit For
                    Next
                End If

                state = True
                Return
            End If
        Next

        If CheckBox2.Checked Then
            If sender Is Button1 Then
                index = 0
            ElseIf sender Is Button2 Then
                index = MainWindow.ListView1.Items.Count - 1
            Else
                Return
            End If

            GoTo REWORK
        Else
            sender.Enabled = False
        End If
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

    End Sub
#End Region
End Class