﻿'Imports Microsoft.WindowsAPICodePack.Taskbar
'Imports Microsoft.WindowsAPICodePack.Shell
Public Module Utils
    Friend R2L As Integer = 0
    'Friend JumpList As JumpList

    Friend CommandLine As String()

    Friend Config As iniFile
    Friend Recent As List(Of String)

    Friend Language As iniFile
    Friend Languages As Dictionary(Of String, String)

    Friend Highlights As List(Of TranslatorIO.HighlightGroup)

    Friend DB As New TranslatorIO.DataBase(IO.Path.Combine(Application.StartupPath, "ShererData.UDB"))

    Friend MsgBoxOptions As MessageBox.MsgBoxOptions
    'Friend MsgBox As New Sherer.Forms.MsgBox(My.Application.Info.Title)
    Friend Const OkCancel As MsgBoxStyle = MsgBoxStyle.OkCancel Or MsgBoxStyle.Question
    Friend Const OkCancelDefault1 As MsgBoxStyle = OkCancel Or MsgBoxStyle.DefaultButton1
    Friend Const OkCancelDefault2 As MsgBoxStyle = OkCancel Or MsgBoxStyle.DefaultButton2

    Friend Const SplitStr As String = ",./\|`~!@#$%^&*()_+-=[]{};':<>?،؟؛»«，。？！‘；、【】（）"""

    Friend Function getTranslateInfo() As KeyValuePair(Of String, String)
        Dim lan As String = Config("ui", "language", "ug")
        Dim source As String = Config("settings", "source_language", "auto")
        Dim target As String = Config("settings", "source_language", "")
        If String.IsNullOrEmpty(source) OrElse Not Languages.ContainsKey(source) Then source = "auto"
        If String.IsNullOrEmpty(target) OrElse Not Languages.ContainsKey(target) Then target = lan
        target = If(target = "cn", "zh-CN", target)

        Return New KeyValuePair(Of String, String)(source, target)
    End Function

End Module