﻿Imports System.ComponentModel

Public Class SuggestList

    Private _backcolor As Color
    Private _hovercolor As Color = Color.FromArgb(225, 228, 231)
    Private _marginHeight As Integer = 5
    Private _tagFont As Font

    Private Sub SetColor()
        Dim x As Integer = MousePosition.X
        Dim y As Integer = MousePosition.Y

        For Each control As Control In Me.Controls
            Dim pos As Point = control.PointToScreen(New Point(0, 0))
            Dim size As Size = control.Size

            If control.Tag <> "margin" AndAlso x > pos.X AndAlso x < pos.X + size.Width AndAlso y > pos.Y AndAlso y < pos.Y + size.Height Then
                control.BackColor = Me.HoverColor
            Else
                control.BackColor = Me.BackColor
            End If
        Next
    End Sub
    Public Sub Add(text As String, tag1 As String, tag2 As String, tag3 As String, rtl As RightToLeft, onClick As EventHandler)
        Dim panel1 As New TableLayoutPanel
        panel1.RowCount = 2
        panel1.ColumnCount = 1
        panel1.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 100.0!))
        panel1.RowStyles.Add(New RowStyle(SizeType.AutoSize))
        panel1.RowStyles.Add(New RowStyle(SizeType.AutoSize))
        panel1.Dock = DockStyle.Top
        panel1.AutoSize = True
        panel1.Width = Me.Width
        AddHandler panel1.MouseMove, AddressOf SetColor
        AddHandler panel1.MouseLeave, AddressOf SetColor
        AddHandler panel1.Click, onClick

        Dim panel2 As New TableLayoutPanel
        panel2.RowCount = 1
        panel2.ColumnCount = 5
        panel2.RowStyles.Add(New RowStyle(SizeType.AutoSize))
        panel2.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
        panel2.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
        panel2.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
        'panel2.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
        panel2.ColumnStyles.Add(New ColumnStyle(SizeType.Percent))
        panel2.Anchor = AnchorStyles.Top Or AnchorStyles.Bottom Or AnchorStyles.Left Or AnchorStyles.Right
        panel2.AutoSize = True
        panel2.Margin = New Padding(0)
        panel2.Padding = New Padding(3, 5, 3, 5)
        AddHandler panel2.MouseMove, AddressOf SetColor
        AddHandler panel2.MouseLeave, AddressOf SetColor
        AddHandler panel2.Click, onClick

        Dim label As New Label
        label.Text = text
        label.AutoSize = True
        label.RightToLeft = rtl
        label.Margin = New Padding(3, 5, 3, 5)
        label.Anchor = AnchorStyles.Left Or AnchorStyles.Right
        AddHandler label.MouseMove, AddressOf SetColor
        AddHandler label.MouseLeave, AddressOf SetColor
        AddHandler label.Click, onClick

        Dim label1 As New Label
        label1.Text = tag1
        label1.AutoSize = True
        label1.Font = Me.TagFont
        label1.Anchor = AnchorStyles.None
        label1.ForeColor = Color.FromArgb(109, 109, 109)
        AddHandler label1.MouseMove, AddressOf SetColor
        AddHandler label1.MouseLeave, AddressOf SetColor
        AddHandler label1.Click, onClick

        Dim label2 As New Label
        label2.Text = tag2
        label2.AutoSize = True
        label2.Font = Me.TagFont
        label2.Anchor = AnchorStyles.None
        label2.ForeColor = Color.FromArgb(109, 109, 109)
        AddHandler label2.MouseMove, AddressOf SetColor
        AddHandler label2.MouseLeave, AddressOf SetColor
        AddHandler label2.Click, onClick

        Dim label3 As New Label
        label3.Text = tag3
        label3.AutoSize = True
        label3.Font = Me.TagFont
        label3.Anchor = AnchorStyles.None
        label3.ForeColor = Color.FromArgb(167, 167, 167)
        AddHandler label3.MouseMove, AddressOf SetColor
        AddHandler label3.MouseLeave, AddressOf SetColor
        AddHandler label3.Click, onClick

        'Dim label4 As New Label
        'label4.Text = tag4
        'label4.AutoSize = True
        'label4.Font = Me.TagFont
        'label4.Anchor = AnchorStyles.None
        'AddHandler label4.MouseMove, AddressOf SetColor
        'AddHandler label4.MouseLeave, AddressOf SetColor
        'AddHandler label4.Click, onClick

        panel2.Controls.Add(label1, 0, 0)
        panel2.Controls.Add(label2, 1, 0)
        panel2.Controls.Add(label3, 2, 0)
        'panel2.Controls.Add(label4, 3, 0)

        panel1.Controls.Add(label, 0, 0)
        panel1.Controls.Add(panel2, 0, 1)

        Dim margin As New Panel
        margin.Dock = DockStyle.Top
        margin.Height = Me.MarginHeight
        margin.Tag = "margin"

        Me.Controls.Add(panel1)
        Me.Controls.Add(margin)
        panel1.BringToFront()
        margin.BringToFront()
    End Sub
    Public Sub Clear()
        Me.Controls.Clear()
    End Sub
    Private Sub SuggestList_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        Call SetColor()
    End Sub
    Private Sub SuggestList_MouseLeave(sender As Object, e As EventArgs) Handles Me.MouseLeave
        Call SetColor()
    End Sub

    Public ReadOnly Property Count As Integer
        Get
            Return Me.Controls.Count
        End Get
    End Property
    <EditorBrowsable(EditorBrowsableState.Always), Browsable(True),
     DesignerSerializationVisibility(DesignerSerializationVisibility.Visible), Bindable(True)>
    Public Overloads Property BackColor As Color
        Get
            Return _backcolor
        End Get
        Set(value As Color)
            _backcolor = value
        End Set
    End Property
    <DefaultValue("225, 228, 231"), Bindable(True)>
    Public Property HoverColor As Color
        Get
            Return _hovercolor
        End Get
        Set(value As Color)
            _hovercolor = value
        End Set
    End Property
    <Bindable(True)>
    Public Property MarginHeight As Integer
        Get
            Return _marginHeight
        End Get
        Set(value As Integer)
            _marginHeight = value
        End Set
    End Property
    <Bindable(True)>
    Public Property TagFont As Font
        Get
            Return _tagFont
        End Get
        Set(value As Font)
            _tagFont = value
        End Set
    End Property
End Class