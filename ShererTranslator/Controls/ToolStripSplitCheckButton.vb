﻿Imports System.ComponentModel
''' <summary>
''' ToolStripSplitCheckButton adds a Check property to a ToolStripSplitButton.
''' </summary>
Partial Public Class ToolStripSplitCheckButton
    Inherits ToolStripSplitButton
    '==============================================================================
    ' Inner class: ToolBarButonSplitCheckButtonEventArgs
    '==============================================================================

    ''' <summary>
    ''' The event args for the check button click event. To be able to use the OnCheckedChanged
    ''' event, we must also record a dummy button as well as this one (hack).
    ''' </summary>
    Public Class ToolBarButonSplitCheckButtonEventArgs
        Inherits ToolBarButtonClickEventArgs
        ''' <summary>
        ''' Constructor.
        ''' </summary>
        ''' <param name="split_button">The sender split check button</param>
        Public Sub New(split_button As ToolStripSplitCheckButton)
            MyBase.New(New ToolBarButton("Dummy Button"))
            ' Hack - Dummy Button is not used
            SplitCheckButton = split_button
        End Sub

        ''' <summary>
        ''' The ToolStripSplitCheckButton to be sent as an argument.
        ''' </summary>
        Public Property SplitCheckButton() As ToolStripSplitCheckButton
    End Class


    '==========================================================================
    ' Construction

    Public Sub New()
        m_checked = False
        m_mouse_over = False
    End Sub


    '==========================================================================
    ' Properties

    ''' <summary>
    ''' Indicates whether the button should toggle its Checked state on click.
    ''' </summary>
    <Category("Behavior"), Description("Indicates whether the item should toggle its selected state when clicked."), DefaultValue(True)>
    Public Property CheckOnClick() As Boolean

    ''' <summary>
    ''' Indictates the Checked state of the button.
    ''' </summary>
    <Category("Behavior"), Description("Indicates whether the ToolStripSplitCheckButton is pressed in or not pressed in."), DefaultValue(False)>
    Public Property Checked() As Boolean
        Get
            Return m_checked
        End Get
        Set
            m_checked = Value
        End Set
    End Property


    '==========================================================================
    ' Methods

    ''' <summary>
    ''' Toggle the click state on button click.
    ''' </summary>
    Protected Overrides Sub OnButtonClick(e As EventArgs)
        If CheckOnClick Then
            m_checked = Not m_checked
            ' Raise the OnCheckStateChanged event when the button is clicked
            'If OnCheckChanged IsNot Nothing Then
            Dim args As New ToolBarButonSplitCheckButtonEventArgs(Me)
            RaiseEvent OnCheckChanged(Me, args)
            'End If
        End If
        MyBase.OnButtonClick(e)
    End Sub

    ''' <summary>
    ''' On mouse enter, record that we are over the button.
    ''' </summary>
    Protected Overrides Sub OnMouseEnter(e As EventArgs)
        m_mouse_over = True
        MyBase.OnMouseEnter(e)
        Me.Invalidate()
    End Sub

    ''' <summary>
    ''' On mouse leave, record that we are no longer over the button.
    ''' </summary>
    Protected Overrides Sub OnMouseLeave(e As EventArgs)
        m_mouse_over = False
        MyBase.OnMouseLeave(e)
        Me.Invalidate()
    End Sub

    ''' <summary>
    ''' Paint the check highlight when required.
    ''' </summary>
    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        MyBase.OnPaint(e)
        If m_checked Then
            ' I can't get the check + mouse over to render properly, so just give the button a colour fill - Hack
            If m_mouse_over Then
                Using brush As Brush = New SolidBrush(Color.FromArgb(64, SystemColors.MenuHighlight))
                    e.Graphics.FillRectangle(brush, ButtonBounds)
                End Using
            End If
            ' To draw around the button + drop-down
            'this.ButtonBounds,        // To draw only around the button 
            ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, SystemColors.MenuHighlight, ButtonBorderStyle.Solid)
        End If
    End Sub


    '==========================================================================
    ' Member Variables

    ' The delegate that acts as a signature for the function that is ultimately called 
    ' when the OnCheckChanged event is triggered.
    Public Delegate Sub SplitCheckButtonEventHandler(source As Object, e As EventArgs)
    Public Shadows Event OnCheckChanged As SplitCheckButtonEventHandler

    Private m_checked As Boolean
    Private m_mouse_over As Boolean
End Class