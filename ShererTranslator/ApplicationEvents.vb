﻿Imports TranslatorIO
Imports Newtonsoft.Json.JsonConvert
Imports Microsoft.VisualBasic.ApplicationServices
Namespace My
    ' The following events are available for MyApplication:
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
    Partial Friend Class MyApplication
        Private Sub MyApplication_Startup(sender As Object, e As StartupEventArgs) Handles Me.Startup
            'Read command line
            CommandLine = e.CommandLine.ToArray

            'Get Application path
            Dim path As String = Windows.Forms.Application.StartupPath

            'Load config file
            Config = New iniFile(IO.Path.Combine(path, "config.ini"), True, True, True)

            'Load language file
            Language = New iniFile(IO.Path.Combine(path, $"Languages/{Config("ui", "language", "en")}.ini"), True, True, True)
            R2L = Val(Language("language", "rtl", "0"))
            R2L = If(R2L = 1, 1, 0)

            'Load languages info
            Languages = New Dictionary(Of String, String)
            For Each file In IO.Directory.GetFiles(IO.Path.Combine(path, "Languages"))
                Dim temp As New iniFile(file)
                Dim fileName As String = GetFileName(file)
                Dim languageName As String = Trim(temp("language", "name", ""))
                If languageName <> "" AndAlso languageName = Trim(temp("languages", fileName, "")) Then
                    Languages.Add(fileName, Language("languages", fileName, languageName) & " - " & languageName)
                End If
            Next

            'Set msgbox
            MsgBoxOptions = New MessageBox.MsgBoxOptions(R2L,
                New FontConverter().ConvertFromString(Language("fonts", "message")),
                New FontConverter().ConvertFromString(Language("fonts", "message_btn")),
                Language("message", "ok"), Language("message", "yes"),
                Language("message", "no"), Language("message", "abort"),
                Language("message", "retry"), Language("message", "ignore"),
                Language("message", "cancel"), Language("message", "help"))
            'MsgBox.Options = MsgBoxOptions.Clone()

            'Set highlight
            Highlights = New List(Of HighlightGroup)
            Dim content As String = Config("info", "highlight", "")
            Try : Highlights = DeserializeObject(Of List(Of HighlightGroup))(content) : Catch ex As Exception : End Try

            'Load recent
            Recent = New List(Of String)
            Recent = Config("info", "recent", "").Split({"|"}, StringSplitOptions.RemoveEmptyEntries).ToList()

            'Load last edit file
            Dim last As String = Trim(Config("info", "last", ""))
            If CommandLine.Length = 0 AndAlso last <> "" AndAlso IO.File.Exists(last) Then CommandLine = {last}
        End Sub
    End Class
End Namespace
