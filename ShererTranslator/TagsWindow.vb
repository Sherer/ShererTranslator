﻿Public Class TagsWindow

#Region "Variables"
    Dim actived As Boolean
    Dim threeCheck As Boolean
    Public CheckTags As New List(Of String)
    Public UncheckTags As New List(Of String)
    Public IndeterminateTags As New List(Of String)
#End Region

#Region "UI Language"
    Sub New()
        Me.New(Nothing, True)
    End Sub
    Sub New(canAdd As Boolean)
        Me.New(Nothing, canAdd)
    End Sub
    Sub New(title As String, Optional canAdd As Boolean = True, Optional threeCheckState As Boolean = True)

#Region "This call is required by the designer."
        actived = False
        InitializeComponent()
        threeCheck = threeCheckState
        TableLayoutPanel2.Visible = canAdd
        actived = True
#End Region

#Region "Set fonts"
        Dim converter As New FontConverter
        Dim generalFont As Font = converter.ConvertFromString(Language("fonts", "general"))
        Dim titleFont As Font = converter.ConvertFromString(Language("fonts", "title"))

        Me.Font = generalFont
        Label1.Font = titleFont
#End Region

#Region "Set right to left"
        Me.RightToLeft = R2L
        Me.RightToLeftLayout = R2L
#End Region

#Region "Set window words"
        ''Basic
        Me.Text = Language("tags", "title")
        Me.Button1.Text = Language("tags", "ok")
        Me.Button2.Text = Language("tags", "add")
        Me.Label1.Text = If(title, Language("tags", "tip_title"))
#End Region

    End Sub
    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles CheckedListBox1.ItemCheck
        If threeCheck Then
            Select Case e.CurrentValue
                Case CheckState.Unchecked : e.NewValue = CheckState.Checked
                Case CheckState.Checked : e.NewValue = CheckState.Indeterminate
                Case CheckState.Indeterminate : e.NewValue = CheckState.Unchecked
            End Select
        End If
    End Sub
    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then Button2.PerformClick() : e.SuppressKeyPress = True
    End Sub
#End Region

#Region "Basic"
    Private Sub TagsWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckedListBox1.Items.Clear()
        For Each tag As String In DB.Tags
            CheckedListBox1.Items.Add(tag, CheckState.Checked)
        Next
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CheckTags = New List(Of String)
        UncheckTags = New List(Of String)
        IndeterminateTags = New List(Of String)

        For i = 0 To CheckedListBox1.Items.Count - 1
            Select Case CheckedListBox1.GetItemCheckState(i)
                Case CheckState.Checked : CheckTags.Add(CheckedListBox1.Items(i))
                Case CheckState.Unchecked : UncheckTags.Add(CheckedListBox1.Items(i))
                Case CheckState.Indeterminate : IndeterminateTags.Add(CheckedListBox1.Items(i))
            End Select
        Next

        Me.DialogResult = DialogResult.OK
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim tag As String = Trim(TextBox1.Text)
        If tag = "" Then
            Return
        ElseIf CheckedListBox1.Items.Contains(tag) Then
            CheckedListBox1.SelectedItem = tag
        Else
            CheckedListBox1.Items.Add(tag, CheckState.Checked)
            CheckedListBox1.SelectedIndex = CheckedListBox1.Items.Count - 1
            TextBox1.Clear()
        End If
    End Sub
#End Region
End Class