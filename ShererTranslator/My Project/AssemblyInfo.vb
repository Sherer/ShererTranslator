﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Sherer Translator")>
<Assembly: AssemblyDescription("Localization & translation tools")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("Sherer Translator tools")>
<Assembly: AssemblyCopyright("Copyright © 2019 Sherer")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("55c6a07b-4107-4250-8d56-e0ff9ebf59fc")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("2.1.507.2021")>
<Assembly: AssemblyFileVersion("2.1.507.2021")>
