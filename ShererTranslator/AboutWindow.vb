﻿Public NotInheritable Class AboutWindow

#Region "Variables"
    Dim actived As Boolean
#End Region

#Region "UI Language"
    Sub New()

#Region "This call is required by the designer."
        actived = False
        InitializeComponent()
        actived = True
#End Region

#Region "Set fonts"
        Dim converter As New FontConverter
        Dim welcomeFont As Font = converter.ConvertFromString(Language("fonts", "welcome"))
        Dim welcomelargeFont As Font = converter.ConvertFromString(Language("fonts", "welcome_large"))
        Dim welcomeSmallFont As Font = converter.ConvertFromString(Language("fonts", "welcome_small"))

        Me.Font = welcomeFont
        Label1.Font = welcomelargeFont
        Label3.Font = welcomeSmallFont
        LinkLabel1.Font = welcomeSmallFont
#End Region

#Region "Set right to left"
        Me.RightToLeft = R2L
        Me.RightToLeftLayout = R2L
#End Region

#Region "Set window words"
        ''Basic
        Me.Text = Language("about", "title")
        Me.Label1.Text = Language("about", "name")
        Me.Label2.Text = String.Format(Language("about", "version"), My.Application.Info.Version.ToString(3), My.Application.Info.Version.Revision)
        Me.LinkLabel1.Text = Language("about", "open_source")
        Me.Label3.Text = My.Application.Info.Copyright
        Me.Button1.Text = Language("about", "ok")
#End Region

    End Sub
#End Region

    Private Sub LinkLabel1_MouseHover(sender As Object, e As EventArgs) Handles LinkLabel1.MouseHover
        LinkLabel1.LinkVisited = True
    End Sub
    Private Sub LinkLabel1_MouseLeave(sender As Object, e As EventArgs) Handles LinkLabel1.MouseLeave
        LinkLabel1.LinkVisited = False
    End Sub
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Process.Start("https://gitee.com/Sherer/ShererTranslator")
    End Sub
End Class
