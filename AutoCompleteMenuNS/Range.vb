﻿Imports System
Imports System.Text
Imports System.Windows.Forms
Imports System.Collections.Generic

Public Class Range

    Private _targetWrapper As ITextBoxWrapper

    Public Property TargetWrapper() As ITextBoxWrapper
        Get
            Return _targetWrapper
        End Get
        Private Set(value As ITextBoxWrapper)
            _targetWrapper = value
        End Set
    End Property
    Public Property Start() As Integer
    Public Property [End]() As Integer
    Public Property Text() As String
        Get
            Dim _text = TargetWrapper.Text
            If String.IsNullOrEmpty(_text) Then Return ""
            If Start >= _text.Length Then Return ""
            If [End] > _text.Length Then Return ""

            Return TargetWrapper.Text.Substring(Start, [End] - Start)
        End Get
        Set(value As String)
            TargetWrapper.SelectionStart = Start
            TargetWrapper.SelectionLength = [End] - Start
            TargetWrapper.SelectedText = value
        End Set
    End Property

    Public Sub New(targetWrapper As ITextBoxWrapper)
        Me.TargetWrapper = targetWrapper
    End Sub

End Class