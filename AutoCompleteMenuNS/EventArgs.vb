﻿Imports System
Imports System.Text
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Collections.Generic

Public Class SelectingEventArgs
    Inherits EventArgs

    Private _item As AutoCompleteItem

    Public Property Item() As AutoCompleteItem
        Get
            Return _item
        End Get
        Friend Set(value As AutoCompleteItem)
            _item = value
        End Set
    End Property
    Public Property Cancel() As Boolean
    Public Property SelectedIndex() As Integer
    Public Property Handled() As Boolean
End Class

Public Class SelectedEventArgs
    Inherits EventArgs

    Private _item As AutoCompleteItem

    Public Property Item() As AutoCompleteItem
        Get
            Return _item
        End Get
        Friend Set(value As AutoCompleteItem)
            _item = value
        End Set
    End Property
    Public Property Control() As Control
End Class

Public Class HoveredEventArgs
    Inherits EventArgs

    Private _item As AutoCompleteItem

    Public Property Item() As AutoCompleteItem
        Get
            Return _item
        End Get
        Friend Set(value As AutoCompleteItem)
            _item = value
        End Set
    End Property
End Class

Public Class PaintItemEventArgs
    Inherits PaintEventArgs

    Private _textRect As RectangleF
    Private _stringFormat As StringFormat
    Private _font As Font
    Private _isSelected As Boolean
    Private _isHovered As Boolean
    Private _colors As Colors

    Public Property TextRect() As RectangleF
        Get
            Return _textRect
        End Get
        Friend Set(value As RectangleF)
            _textRect = value
        End Set
    End Property
    Public Property StringFormat() As StringFormat
        Get
            Return _stringFormat
        End Get
        Friend Set(value As StringFormat)
            _stringFormat = value
        End Set
    End Property
    Public Property Font() As Font
        Get
            Return _font
        End Get
        Friend Set(value As Font)
            _font = value
        End Set
    End Property
    Public Property IsSelected() As Boolean
        Get
            Return _isSelected
        End Get
        Friend Set(value As Boolean)
            _isSelected = value
        End Set
    End Property
    Public Property IsHovered() As Boolean
        Get
            Return _isHovered
        End Get
        Friend Set(value As Boolean)
            _isHovered = value
        End Set
    End Property
    Public Property Colors() As Colors
        Get
            Return _colors
        End Get
        Friend Set(value As Colors)
            _colors = value
        End Set
    End Property

    Public Sub New(graphics As Graphics, clipRect As Rectangle)
        MyBase.New(graphics, clipRect)
    End Sub
End Class

Public Class WrapperNeededEventArgs
    Inherits EventArgs

    Private _targetControl As Control

    Public Property TargetControl() As Control
        Get
            Return _targetControl
        End Get
        Private Set(value As Control)
            _targetControl = value
        End Set
    End Property
    Public Property Wrapper() As ITextBoxWrapper

    Public Sub New(targetControl As Control)
        Me.TargetControl = targetControl
    End Sub
End Class