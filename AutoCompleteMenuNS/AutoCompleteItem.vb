﻿Imports System
Imports System.Text
Imports System.Drawing

''' <summary>
''' Item of AutoCompleteMenu
''' </summary>
Public Class AutoCompleteItem

    Public Tag As Object
    Private _text As String
    Private _toolTipTitle As String
    Private _toolTipText As String
    Private _menuText As String
    Private _parent As AutoCompleteMenu

    ''' <summary>
    ''' Parent AutoCompleteMenu
    ''' </summary>
    Public Property Parent() As AutoCompleteMenu
        Get
            Return _parent
        End Get
        Friend Set(value As AutoCompleteMenu)
            _parent = value
        End Set
    End Property

    ''' <summary>
    ''' Text for inserting into textbox
    ''' </summary>
    Public Property Text() As String
        Get
            Try
                Return If(String.IsNullOrEmpty(_text), _text, RegularExpressions.Regex.Unescape(_text))
            Catch ex As Exception
                Return If(String.IsNullOrEmpty(_text), _text, _text)
            End Try
        End Get
        Set(value As String)
            _text = value
        End Set
    End Property

    ''' <summary>
    ''' Image index for this item
    ''' </summary>
    Public Property ImageIndex() As Integer

    ''' <summary>
    ''' Title for tooltip.
    ''' </summary>
    ''' <remarks>Return null for disable tooltip for this item</remarks>
    Public Overridable Property ToolTipTitle() As String
        Get
            Return If(String.IsNullOrEmpty(_toolTipTitle), _toolTipTitle, RegularExpressions.Regex.Unescape(_toolTipTitle))
        End Get
        Set(value As String)
            _toolTipTitle = value
        End Set
    End Property

    ''' <summary>
    ''' Tooltip text.
    ''' </summary>
    ''' <remarks>For display tooltip text, ToolTipTitle must be not null</remarks>
    Public Overridable Property ToolTipText() As String
        Get
            Return If(String.IsNullOrEmpty(_toolTipText), _toolTipText, RegularExpressions.Regex.Unescape(_toolTipText))
        End Get
        Set(value As String)
            _toolTipText = value
        End Set
    End Property

    ''' <summary>
    ''' Menu text. This text is displayed in the drop-down menu.
    ''' </summary>
    Public Overridable Property MenuText() As String
        Get
            Return If(String.IsNullOrEmpty(_menuText), _menuText, RegularExpressions.Regex.Unescape(_menuText))
        End Get
        Set(value As String)
            _menuText = value
        End Set
    End Property

    Public Sub New()
        ImageIndex = -1
    End Sub
    Public Sub New(text As String)
        Me.New()
        Me.Text = text
    End Sub
    Public Sub New(text As String, imageIndex As Integer)
        Me.New(text)
        Me.ImageIndex = imageIndex
    End Sub
    Public Sub New(text As String, imageIndex As Integer, menuText As String)
        Me.New(text, imageIndex)
        Me.MenuText = menuText
    End Sub
    Public Sub New(text As String, imageIndex As Integer, menuText As String, toolTipTitle As String, toolTipText As String)
        Me.New(text, imageIndex, menuText)
        Me.ToolTipTitle = toolTipTitle
        Me.ToolTipText = toolTipText
    End Sub

    ''' <summary>
    ''' Returns text for inserting into Textbox
    ''' </summary>
    Public Overridable Function GetTextForReplace() As String
        Return Text
    End Function

    ''' <summary>
    ''' Compares fragment text with this item
    ''' </summary>
    Public Overridable Function Compare(fragmentText As String) As CompareResult
        If Text.StartsWith(fragmentText, StringComparison.InvariantCultureIgnoreCase) AndAlso Text <> fragmentText Then
            Return CompareResult.VisibleAndSelected
        End If

        Return CompareResult.Hidden
    End Function

    ''' <summary>
    ''' Returns text for display into popup menu
    ''' </summary>
    Public Overrides Function ToString() As String
        Return If(MenuText, Text)
    End Function

    ''' <summary>
    ''' This method is called after item was inserted into text
    ''' </summary>
    Public Overridable Sub OnSelected(e As SelectedEventArgs)
    End Sub

    Public Overridable Sub OnPaint(e As PaintItemEventArgs)
        Using brush = New SolidBrush(If(e.IsSelected, e.Colors.SelectedForeColor, e.Colors.ForeColor))
            e.Graphics.DrawString(ToString(), e.Font, brush, e.TextRect, e.StringFormat)
        End Using
    End Sub
End Class

Public Enum CompareResult
    ''' <summary>
    ''' Item do not appears
    ''' </summary>
    Hidden
    ''' <summary>
    ''' Item appears
    ''' </summary>
    Visible
    ''' <summary>
    ''' Item appears and will selected
    ''' </summary>
    VisibleAndSelected
End Enum