﻿Imports System
Imports System.Text
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Collections.Generic

''' <summary>
''' Wrapper over the control like TextBox.
''' </summary>
Public Interface ITextBoxWrapper
    ReadOnly Property TargetControl() As Control
    ReadOnly Property Text() As String
    Property SelectedText() As String
    Property SelectionLength() As Integer
    Property SelectionStart() As Integer
    Function GetPositionFromCharIndex(pos As Integer) As Point
    ReadOnly Property [ReadOnly]() As Boolean
    Event LostFocus As EventHandler
    Event Scroll As ScrollEventHandler
    Event KeyDown As KeyEventHandler
    Event MouseDown As MouseEventHandler
End Interface