﻿Imports System
Imports System.Text
Imports System.Drawing
Imports System.Collections.Generic

<Serializable>
Public Class Colors

    Public Property ForeColor() As Color
    Public Property BackColor() As Color
    Public Property SelectedForeColor() As Color
    Public Property SelectedBackColor() As Color
    Public Property SelectedBackColor2() As Color
    Public Property HighlightingColor() As Color

    Public Sub New()
        ForeColor = Color.Black
        BackColor = Color.White
        SelectedForeColor = Color.Black
        SelectedBackColor = Color.Orange
        SelectedBackColor2 = Color.White
        HighlightingColor = Color.Orange
    End Sub
End Class