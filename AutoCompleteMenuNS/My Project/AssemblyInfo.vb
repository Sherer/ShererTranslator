﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("AutoCompleteMenu")>
<Assembly: AssemblyDescription("AutoCompleteMenu control")>
<Assembly: AssemblyCompany("Pavel Torgashov")>
<Assembly: AssemblyProduct("AutoCompleteMenu")>
<Assembly: AssemblyCopyright("© Pavel Torgashov, 2012-2015, p_torgashov@ukr.net.")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("408f3d7c-4b43-4dd6-abeb-9d406f0506fc")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.3.5.2")>
<Assembly: AssemblyFileVersion("1.3.5.2")>
