﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Collections.Generic

''' <summary>
''' Control for displaying menu items, hosted in AutoCompleteMenu.
''' </summary>
Public Interface IAutoCompleteListView
    ''' <summary>
    ''' Image list
    ''' </summary>
    Property ImageList() As ImageList

    ''' <summary>
    ''' Index of current selected item
    ''' </summary>
    Property SelectedItemIndex() As Integer

    ''' <summary>
    ''' Index of current selected item
    ''' </summary>
    Property HighlightedItemIndex() As Integer

    ''' <summary>
    ''' List of visible elements
    ''' </summary>
    Property VisibleItems() As IList(Of AutoCompleteItem)

    ''' <summary>
    ''' Duration (ms) of tooltip showing
    ''' </summary>
    Property ToolTipDuration() As Integer

    ''' <summary>
    ''' Display tooltip direction
    ''' </summary>
    Property ToolTipRightToLeft() As RightToLeft

    ''' <summary>
    ''' Display tooltip font
    ''' </summary>
    Property ToolTipFont() As Font

    ''' <summary>
    ''' Display tooltip title font
    ''' </summary>
    Property ToolTipTitleFont As Font

    ''' <summary>
    ''' Occurs when user selected item for inserting into text
    ''' </summary>
    Event ItemSelected As EventHandler

    ''' <summary>
    ''' Occurs when current hovered item is changing
    ''' </summary>
    Event ItemHovered As EventHandler(Of HoveredEventArgs)

    ''' <summary>
    ''' Shows tooltip
    ''' </summary>
    ''' <param name="autoCompleteItem"></param>
    ''' <param name="control"></param>
    Sub ShowToolTip(autoCompleteItem As AutoCompleteItem, Optional control As Control = Nothing)

    ''' <summary>
    ''' Hide tooltip
    ''' </summary>
    Sub HideToolTip()

    ''' <summary>
    ''' Returns rectangle of item
    ''' </summary>
    Function GetItemRectangle(itemIndex As Integer) As Rectangle

    ''' <summary>
    ''' Colors
    ''' </summary>
    Property Colors() As Colors
End Interface